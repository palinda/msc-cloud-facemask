import base64
import errno
import os

import boto3
import botocore


class S3FileProvider:

    def __init__(self):
        self.client, self.s3 = boto3.client('s3', region_name='us-east-1'), boto3.resource('s3', region_name='us-east-1')

    def get_file_url(self, bucket, filepath):
        params = {'Bucket': bucket, 'Key': filepath}
        url = self.client.generate_presigned_url('get_object', params)
        return url

    def upload_file_s3(self, bucket, local_file, remote_file):
        transfer = boto3.s3.transfer.S3Transfer(client=self.client)
        return transfer.upload_file(local_file,
                                    bucket,
                                    remote_file
                                    )

    def download_file_s3(self, bucket, file_name):

        # file_name = super().working_dir + file_name
        if '/' in file_name and not os.path.exists(os.path.dirname(file_name)):
            try:
                os.makedirs(os.path.dirname(file_name))
            except OSError as exc:  # Guard against race condition
                if exc.errno != errno.EEXIST:
                    raise

        transfer = boto3.s3.transfer.S3Transfer(client=self.client)
        try:
            transfer.download_file(bucket,
                                   file_name,
                                   file_name
                                   )
        except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] == "404":
                return False
            else:
                raise

        return True

    def download_if_not_exist(self, bucket, file_name):
        if os.path.isfile(file_name):
            return True
        return self.download_file_s3(bucket, file_name)

    def download_file(self, bucket, filename):
        filepath = filename
        if os.path.isfile(filepath):
            return filepath
        self.download_file_s3(bucket, filepath)
        return filepath

    def save_and_upload(self, bucket, filename, file_body):
        filename = filename
        if not os.path.exists(os.path.dirname(filename)):
            try:
                os.makedirs(os.path.dirname(filename))
            except OSError as exc:  # Guard against race condition
                if exc.errno != errno.EEXIST:
                    raise
        if isinstance(file_body, str):
            image_64_decode = base64.b64decode(file_body)
            image_result = open(filename,
                                'wb')  # create a writable image and write the decoding result
            image_result.write(image_64_decode)
            image_result.close()
        else:
            file_body.save(filename)

        self.upload_file_s3(bucket, filename, filename)
        return filename

    def save_locally(self, bucket, filename, file_body):
        filename = filename
        if not os.path.exists(os.path.dirname(filename)):
            try:
                os.makedirs(os.path.dirname(filename))
            except OSError as exc:  # Guard against race condition
                if exc.errno != errno.EEXIST:
                    raise
        if isinstance(file_body, str):
            image_64_decode = base64.b64decode(file_body)
            image_result = open(filename,
                                'wb')  # create a writable image and write the decoding result
            image_result.write(image_64_decode)
            image_result.close()

        else:
            file_body.save(filename)

        return filename

    def delete_locally(self, filePath):
        if os.path.exists(filePath):
            os.remove(filePath)
        else:
            print("Can not delete the file as it doesn't exists")

    def delete_all(self, bucket, filepath):
        filepath = filepath
        obj = self.s3.Object(bucket, filepath)
        obj.delete()
        self.delete_locally(filepath)

