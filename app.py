from flask import Flask
from flask_restx import Api, Resource, fields
from flask_restx import fields, Resource

from s3_file_provider import S3FileProvider
from detect_mask_image import FaceMaskDetector

app = Flask(__name__)
api = Api(app, version='1.0', title='Face Mask API',
          description='Face Mask API',
          default='Face Mask',
          default_label='Face Mask',
          validate=True
          )

image_request = api.model('Validate', {
    'image_path': fields.String
})

s3Client = S3FileProvider()
bucket = 'msc-group4-cloud-repo'
fm = FaceMaskDetector(s3Client, bucket)


@api.route('/validate')
class DocumentValidator(Resource):

    @api.response(200, 'Success')
    @api.response(400, 'Validation Error')
    @api.doc(body=image_request)
    def post(self):
        print(api.payload['image_path'])
        status = fm.mask_image(api.payload['image_path'])
        return {
            'status': status
        }

@api.route('/healthcheck')
class HealthCheck(Resource):

    @api.response(200, 'Success')
    @api.response(400, 'Error')
    def get(self):
        return {}

if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0")
