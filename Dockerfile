FROM python:3.7-stretch

RUN mkdir /app
ADD requirements.txt /app/
WORKDIR /app

RUN apt update && apt install -y gcc && apt install -y libgtk2.0-dev && pip3 install -r requirements.txt && apt remove -y gcc && apt autoremove -y

ADD . /app

EXPOSE 5000
CMD ["app.py"]
ENTRYPOINT ["python3"]
