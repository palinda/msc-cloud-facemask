# USAGE
# python detect_mask_image.py --image images/pic1.jpeg

# import the necessary packages
from tensorflow.keras.applications.mobilenet_v2 import preprocess_input
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.models import load_model
import numpy as np
import argparse
import cv2
import os

from s3_file_provider import S3FileProvider


class FaceMaskDetector():

	def __init__(self, s3Client, bucket):
		# load our serialized face detector model from disk
		print("[INFO] loading face detector model...")
		prototxtPath = os.path.sep.join(['face_detector', "deploy.prototxt"])
		weightsPath = os.path.sep.join(['face_detector',
										"res10_300x300_ssd_iter_140000.caffemodel"])
		self.net = cv2.dnn.readNet(prototxtPath, weightsPath)

		# load the face mask detector model from disk
		print("[INFO] loading face mask detector model...")
		self.model = load_model('mask_detector.model')
		self.s3Client = s3Client
		self.bucketName = bucket

	def mask_image(self, image_name):

		image_path = self.s3Client.download_file(self.bucketName, image_name)
		image = cv2.imread(image_path)
		orig = image.copy()
		(h, w) = image.shape[:2]

		# construct a blob from the image
		blob = cv2.dnn.blobFromImage(image, 1.0, (300, 300),
									 (104.0, 177.0, 123.0))

		# pass the blob through the network and obtain the face detections
		print("[INFO] computing face detections...")
		self.net.setInput(blob)
		detections = self.net.forward()

		# loop over the detections
		face_count = 0
		mask_count = 0
		user_list = [];
		for i in range(0, detections.shape[2]):
			# extract the confidence (i.e., probability) associated with
			# the detection
			confidence = detections[0, 0, i, 2]

			# filter out weak detections by ensuring the confidence is
			# greater than the minimum confidence
			if confidence > 0.5:
				# compute the (x, y)-coordinates of the bounding box for
				# the object
				box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
				(startX, startY, endX, endY) = box.astype("int")

				# ensure the bounding boxes fall within the dimensions of
				# the frame
				(startX, startY) = (max(0, startX), max(0, startY))
				(endX, endY) = (min(w - 1, endX), min(h - 1, endY))

				(cstartX, cstartY) = (max(0, startX - 15), max(0, startY - 15))
				(cendX, cendY) = (min(w - 1, endX + 15), min(h - 1, endY + 15))

				# extract the face ROI, convert it from BGR to RGB channel
				# ordering, resize it to 224x224, and preprocess it
				face = image[startY:endY, startX:endX]
				face = cv2.cvtColor(face, cv2.COLOR_BGR2RGB)
				face = cv2.resize(face, (224, 224))
				face = img_to_array(face)
				face = preprocess_input(face)
				face = np.expand_dims(face, axis=0)

				# pass the face through the model to determine if the face
				# has a mask or not
				(mask, withoutMask) = self.model.predict(face)[0]

				# determine the class label and color we'll use to draw
				# the bounding box and text
				label = "Mask" if mask > withoutMask else "No Mask"
				color = (0, 255, 0) if label == "Mask" else (0, 0, 255)

				face_count += 1
				# if label == 'Mask':
				# 	mask_count += 1

				# include the probability in the label
				label = "{}: {:.2f}%".format(label, max(mask, withoutMask) * 100)

				# display the label and bounding box rectangle on the output
				# frame
				# cv2.putText(image, label, (startX, startY - 10),
				# 			cv2.FONT_HERSHEY_SIMPLEX, 0.45, color, 2)
				# cv2.rectangle(image, (startX, startY), (endX, endY), color, 2)
				name_parts = image_path.split('/')
				name = name_parts[-1]
				updated_path = image_path.replace(name, 'cropped_' + str(face_count) + '_' + name)
				cv2.imwrite(updated_path, image[cstartY:cendY, cstartX:cendX])
				self.s3Client.upload_file_s3(self.bucketName, updated_path, updated_path)
				user_list.append({'file': updated_path, 'status': label})
				# status = 'NO_MASK'face_count
				#
				# if face_count == 0:
				# 	status = 'NO_FACE'
				# elif face_count == mask_count:
				# 	status = 'ALL_MASK'
				# elif mask_count == 0:
				# 	status = 'NO_MASK'
				# else:
				# 	status = 'PARTIAL_MASK'

		return {
			'status': user_list
		}
